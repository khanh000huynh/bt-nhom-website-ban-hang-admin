var productList = [];

const fetchProducts = () => {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
  })
    .then((res) => {
      $("#form")[0].reset();
      mapData(res.data);
      renderProducts();
      renderType();
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchProducts();

const mapData = (data) => {
  productList = [];
  for (let item of data) {
    let product = new Product(
      item.id,
      item.name,
      item.image,
      item.description,
      item.price,
      item.inventory,
      item.rating,
      item.type
    );

    productList.push(product);
  }
};

const renderType = () => {
  let htmlContent = "";
  let type = {};

  htmlContent += `
    <div class="form-group">
      <label for="type">Type:</label>
      <select class="form-control" id="type">
  `;

  for (let product of productList) {
    if (product.type in type) {
      type[product.type] += 1;
    } else {
      type[product.type] = 1;
      htmlContent += `
        <option>${product.type}</option>
      `;
    }
  }

  htmlContent += `
      </select>
    </div>
  `;

  $("#typeSelect").html(htmlContent);
};

const renderProducts = (list = productList) => {
  let htmlContent = "";
  for (let item of list) htmlContent += item.render();
  $("#product-list .row").html(htmlContent);
};

const checkEmpty = (input) => {
  if ($("#" + input).val() === "") {
    $("." + input + "-validation").html("Vui lòng nhập " + input + "!");
    return true;
  } else $("." + input + "-validation").html("");
  return false;
};

const checkId = () => {
  if (checkEmpty("id")) return false;
  let product = productList.find((product) => +product.id === +$("#id").val());
  if (product) {
    $(".id-validation").html("Id này đã tồn tại!");
    return false;
  }
  return true;
};

const addProduct = () => {
  let check = true;
  check &= checkId();
  check &= !checkEmpty("name");
  check &= !checkEmpty("image");
  check &= !checkEmpty("description");
  check &= !checkEmpty("price");
  check &= !checkEmpty("inventory");
  check &= !checkEmpty("rating");
  if (!check) return;

  const product = new Product(
    $("#id").val(),
    $("#name").val(),
    $("#image").val(),
    $("#description").val(),
    $("#price").val(),
    $("#inventory").val(),
    $("#rating").val(),
    $("#type").val()
  );

  console.log(product);

  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "POST",
    data: product,
  })
    .then(() => {
      alert("Đã thêm một sản phẩm mới, id = " + $("#id").val());
      fetchProducts();
    })
    .catch((err) => {
      console.log(err);
    });
};
$(".add-button").on("click", addProduct);

const edit = (id) => {
  // Set validation text to empty
  $(".id-validation").html("");
  $(".name-validation").html("");
  $(".image-validation").html("");
  $(".description-validation").html("");
  $(".price-validation").html("");
  $(".inventory-validation").html("");
  $(".rating-validation").html("");

  let product = productList.find((product) => +product.id === +id);
  $("#id").val(product.id);
  $("#name").val(product.name);
  $("#image").val(product.image);
  $("#description").val(product.description);
  $("#price").val(product.price);
  $("#inventory").val(product.inventory);
  $("#rating").val(product.rating);
  $("#type").val(product.type);

  $("#id").attr("disabled", true);
  $(".add-button").addClass("d-none");
  $(".update-button").removeClass("d-none");
  $(".cancle-button").removeClass("d-none");
};

const updateProduct = () => {
  const product = new Product(
    $("#id").val(),
    $("#name").val(),
    $("#image").val(),
    $("#description").val(),
    $("#price").val(),
    $("#inventory").val(),
    $("#rating").val(),
    $("#type").val()
  );

  axios({
    url:
      "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" +
      $("#id").val(),
    method: "PUT",
    data: product,
  })
    .then((res) => {
      fetchProducts();
      alert("Đã cập nhật thông tin sản phẩm có id = " + $("#id").val());
    })
    .catch((err) => {
      console.log(err);
    });

  $("#id").attr("disabled", false);
  $(".add-button").removeClass("d-none");
  $(".update-button").addClass("d-none");
  $(".cancle-button").addClass("d-none");
};
$(".update-button").on("click", updateProduct);

const cancle = () => {
  $("#id").attr("disabled", false);
  $(".update-button").addClass("d-none");
  $(".add-button").removeClass("d-none");
  $(".cancle-button").addClass("d-none");
  $("#form")[0].reset();
};
$(".cancle-button").on("click", cancle);

const deleteProduct = (id) => {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + +id,
    method: "DELETE",
  })
    .then(() => {
      fetchProducts();
      alert("Đã xóa sản phẩm có id = " + id);
    })
    .catch((err) => {
      console.log(err);
    });
};
