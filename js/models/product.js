class Product {
  constructor(id, name, image, description, price, inventory, rating, type) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.description = description;
    this.price = price;
    this.inventory = inventory;
    this.rating = rating;
    this.type = type;
  }

  render() {
    return `
      <div class="col-sm-12 col-md-6 col-lg-4">
        <img
            src="${this.image}"
        />
        <h2>${this.name}</h2>
        <p>
            ${this.description}
        </p>
        <button class="btn btn-success" onclick="edit(${this.id})">Sửa</button>
        <button class="btn btn-danger" onclick="deleteProduct(${this.id})">Xóa</button>
      </div>
    `;
  }
}
